import React, {Component} from 'react';
import './Todo.css';
class Todo extends Component{
    constructor(){
        super()
        this.state={
            inputText : '',
            tasks : ['To fly away', 'To be or not to be']
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleClick  = this.handleClick.bind(this)
    }
    handleSubmit(event){
        event.preventDefault();
        if (this.state.inputText !== '' && !this.state.tasks.includes(this.state.inputText)){
            let prevTasks = this.state.tasks;
            prevTasks.push(this.state.inputText);
            this.setState({ inputText : '',
                            tasks : prevTasks});
        }
        else
        if(this.state.tasks.includes(this.state.inputText)){
            alert('This task already is in a list');
        }
        else{
            alert("Task shouldn't to be empty");
        }

    }
    handleChange(event){
        const {name,value} = event.target;
        this.setState({[name]: value});
    }
    handleClick(event){
        const {name, value} = event.target;
        console.log("Name: " + name + ", Value: "+value);
        const mvalue = Number(value);
        var prevTasks = this.state.tasks.filter(
            (name,index) => index !== mvalue
        );
        this.setState({
            inputText : this.state.inputText,
            tasks : prevTasks
        });
        alert('Task will be deleted.');
    }
    render(){
        const mystyle = {
            color: "black",
            padding: "3px",
            fontFamily: "Arial"
          };
        const Tasks = this.state.tasks.map(
                (task, index) => <li style = {mystyle} key ={index}>{task} <button onClick = {this.handleClick} name ="deleteValue" value = {index}>x</button></li>
        );
        return(
            <div>
            <form onSubmit={this.handleSubmit}>
                <label id ="text">Task:</label>
                <input id="text" type="text" name ="inputText" placeholder = "Task to do" value={this.state.inputText} onChange={this.handleChange}/>
                <input type="submit" value="Submit"/>
            </form>
            <ul>
                {Tasks}
            </ul>
            </div>

        );
    }
}
;
export default Todo;